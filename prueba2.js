import escapeHtml from './escape-html_edit.js'; // reemplaza caracteres para ser aceptados por la llamada de api
import get_geocode_with_address from './get_geocode_object.js'

var address_test = "Los Militares 6040, Las Condes";

function get_geocod(arr) {
  for (var i = 1; i < arr.length; i+=2) {
    get_geocode_with_address(escapeHtml(arr[i])).then(data => console.log(data));
  }
}

get_geocode_with_address(escapeHtml(address_test)).then(data => console.log(data));

export {get_geocod as default};
