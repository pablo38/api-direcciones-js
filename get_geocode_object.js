import {api_key} from './api_key.js';

//Use your google map api key for check (don' forget to change api_key to other_key)
//var other_key = "<INSERT HERE>"
//Address: Los Militares 6040, Las Condes
//address has to come encoded like (don' forget to change address to address_test)
//var address_test = "Los%20Militares%206040%2C%20Las%20Condes";

async function get_geocode_with_address(address) {
  let response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${api_key}`);
  let data = await response.json();
  return data;
}

//get_geocode_with_address(address_test).then(data => console.log(data));

export{get_geocode_with_address as default};
