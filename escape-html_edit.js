/*!
 * escape-html
 * Copyright(c) 2012-2013 TJ Holowaychuk
 * Copyright(c) 2015 Andreas Lubbe
 * Copyright(c) 2015 Tiancheng "Timothy" Gu
 * MIT Licensed
 * Modificaciones realizadas para cumplir el estandar de google maps api para el traspaso de direcciones
 */

'use strict'

/**
 * Module variables.
 * @private
 */

var matchHtmlRegExp = /[ "+'<>#%|]/

/**
 * Module exports.
 * @public
 */

//module.exports = escapeHtml

/**
 * Escape special characters in the given string of text.
 *
 * @param  {string} string The string to escape for inserting into HTML
 * @return {string}
 * @public
 */

function escapeHtml (string) {
  var str = '' + string
  var match = matchHtmlRegExp.exec(str)

  if (!match) {
    return str
  }

  var escape
  var html = ''
  var index = 0
  var lastIndex = 0

  for (index = match.index; index < str.length; index++) {
    switch (str.charCodeAt(index)) {
      case 32: // [Space]
        escape = '%20'
      case 34: // "
        escape = '%22'
        break
      case 35: // #
        escape = '%23'
        break
      case 37: // %
        escape = '%25'
        break
      case 43: // +
        escape = '%2B'
        break
      case 44: // ,
        escape = '%2C'
        break
      case 60: // <
        escape = '%3C'
        break
      case 62: // >
        escape = '%3E'
        break
      case 124: // |
        escape = '%7C'
        break
      default:
        continue
    }

    if (lastIndex !== index) {
      html += str.substring(lastIndex, index)
    }

    lastIndex = index + 1
    html += escape
  }

  return lastIndex !== index
    ? html + str.substring(lastIndex, index)
    : html
}

export {escapeHtml as default};
